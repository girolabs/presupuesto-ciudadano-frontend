import React from "react";

import { HorizontalBar } from "react-chartjs-2";

const BarraHorizontal = props => {
  const options = {
    legend: {
      display: false
    },
    tooltips: {
      enabled: true,
   
      callbacks: {
        label: function(tooltipItems, data) {
          return (
            data.datasets[tooltipItems.datasetIndex].label+": G. "+
            data.datasets[tooltipItems.datasetIndex].data[
              tooltipItems.index
            ].toFixed(3) + " Billones"
            /* .toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") */
          );
        }
      }
    },
    scales: {
      xAxes: [
        {
          stacked: true,
          ticks: {
            min: 0
          },
          scaleLabel: {
            display: true,
            labelString: 'Monto en Billones de G.'
          },
         
          barPercentage: 0.4,
          //categoryPercentage: 0.5,
        }
      ],
      yAxes: [
        {
          stacked: true,
          ticks: {
            min: 0
        }, 
        
        barPercentage: 0.4,
         // categoryPercentage: 0.5,
        }
        
      ]
    }
  };

  return <HorizontalBar data={props.data} options={options} />;
};

export default BarraHorizontal;
